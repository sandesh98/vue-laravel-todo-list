<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['user_id', 'text'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function checkOrUncheck($task)
    {
        if ($task->done === 0) {
            $task->done = 1;
            $task->save();
        } else {
            $task->done = 0;
            $task->save();
        }
    }
}
