<?php

namespace App\Http\Controllers;

use App\Http\Requests\Task\StoreTask;
use App\Http\Requests\Task\UpdateTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Task;

class TaskController extends Controller
{
    public function store(StoreTask $request)
    {
        $task = Task::create([
            'text' => $request->text,
            'user_id' => auth()->id()
         ]);

        return $task;

    }

    public function index()
    {
        return Auth::user()
            ->tasks()
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return 200;
    }

    public function update(UpdateTask $request, $id)
    {
        Task::find($id)
            ->update($request->only('text'));
    }

    public function checkOrUncheckCheckbox($id)
    {
        $task = Task::findOrFail($id);
        $task->checkOrUncheck($task);
    }
}
