<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new App\User;
        $user->name = 'sandesh';
        $user->email = 'sandeshb981@gmail.com';
        $user->password = bcrypt('123123');
        $user->save();
    }
}
